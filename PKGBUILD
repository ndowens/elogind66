# Maintainer: artoo <artoo@artixlinux.org>

pkgbase=elogind
pkgname=('elogind' 'libelogind')
pkgver=246.10
pkgrel=2
pkgdesc="The systemd project's logind, extracted to a standalone package"
arch=('x86_64')
url="https://github.com/elogind/elogind"
license=('GPL' 'LGPL2.1')
makedepends=('acl' 'intltool' 'libtool' 'gperf' 'gtk-doc' 'polkit' 'dbus'
            'libseccomp' 'meson' 'kexec-tools' 'openrc' 'libcap')
options=('!libtool')
source=("${pkgbase}-${pkgver}.tar.gz::https://github.com/elogind/elogind/archive/v${pkgver}.tar.gz")
sha256sums=('c490dc158c8f5bca8d00ecfcc7ad5af24d1c7b9e59990a0b3b1323996221a922')

prepare() {
    cd ${pkgbase}-${pkgver}
}

build() {
    local meson_options=(
        -Drootlibdir=/usr/lib
        -Drootlibexecdir=/usr/lib/elogind
        -Ddbuspolicydir=/usr/share/dbus-1/system.d
        -Ddocdir=/usr/share/doc/elogind
        -Ddefault-hierarchy=hybrid
        -Dcgroup-controller=none
        -Ddefault-kill-user-processes=false
    )

    arch-meson "$pkgbase-${pkgver}" build "${meson_options[@]}"

    ninja -C build
}

check(){
    meson test -C build --print-errorlogs
}

package_elogind() {
    pkgdesc="The systemd project's logind, extracted to a standalone package"
    provides=("elogind=${pkgver}" "systemd=${pkgver}" 'logind')
    conflicts=('systemd')
    depends=('acl' 'dbus' 'libseccomp' 'libelogind' 'kexec-tools' 'udev')
    optdepends=('polkit: polkit support')
    backup=('etc/elogind/logind.conf')

    DESTDIR="$pkgdir" meson install -C build

    ln -sfv libelogind.pc "${pkgdir}"/usr/lib/pkgconfig/libsystemd.pc

    install -dm755 "${srcdir}"/_libelogind
    mv -v "${pkgdir}"/usr/lib/libelogind*.so* "${srcdir}"/_libelogind

    install -d "${pkgdir}"/{etc,usr/lib}/elogind/{logind,sleep}.conf.d
}

package_libelogind(){
    pkgdesc="elogind client libraries"
    provides=('libelogind.so' "libelogind=${pkgver}" 'liblogind'
            "libsystemd=${pkgver}" "systemd-libs=${pkgver}")
    conflicts=('systemd-libs')
    depends=('libcap' 'libudev')

    cd "${pkgbase}-${pkgver}"

    install -dm755 "${pkgdir}"/usr/lib
    mv "${srcdir}"/_libelogind/libelogind*.so* "${pkgdir}"/usr/lib

    ln -sfv libelogind.so "${pkgdir}"/usr/lib/libsystemd.so
    ln -sfv libsystemd.so "${pkgdir}"/usr/lib/libsystemd.so.0
}
